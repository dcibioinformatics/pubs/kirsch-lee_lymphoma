array=(/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/signature/converted_maf/*)
outdir=/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/signature/merged_maf
dir=/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/signature/converted_maf/
##headline=2
awk 'NR=2 {print}' ${dir}/4014-filtered.maf >>${outdir}/Changlung_merged_converted.maf
for s in "${array[@]}";
do
    echo ${s}
    sid=${s##*/}
    sample=${sid%.maf}
    samplename=${sid%-filtered.maf}
    awk 'NR > 2 {print}' ${s} >> ${outdir}/Changlung_merged_converted.maf 
done
