v2m=~/mskcc-vcf2maf-decbf60/vcf2maf.pl
ref=/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/WholeExome/Data/sanger/GRCm38_68.fa
#array=(/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/WES_2018/VEP/filtered/*)
#array=(/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/Sarcoma_WES/VEP/filtered/*)
array=(/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/WholeExome/VEP/filtered/*)
outdir=/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/signature/converted_maf
tmp=/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/signature/tmp

for s in "${array[@]}";
do
    echo ${s}
    sid=${s##*/}
    sample=${sid%.vcf}
    samplename=${sid%-filtered.vcf}
    
    perl ${v2m} --input-vcf ${s} \
        --tmp-dir ${tmp} \
        --vep-path ~/ensembl-vep/ \
        --vep-data ~/.vep \
        --species mus_musculus \
        --ncbi-build GRCm38 \
        --ref-fasta ${ref} \
        --tumor-id ${samplename}_T \
        --normal-id ${samplename}_L \
        --output-maf ${outdir}/${sample}.maf
done
