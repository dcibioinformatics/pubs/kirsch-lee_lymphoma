
\documentclass[xcolor=x11names,compress]{beamer} 
\mode<presentation> {
    \usetheme{Rochester}
    \usepackage{xcolor, colortbl}
    \definecolor{Green}{gray}{0.6}
    \definecolor{Blue}{gray}{0.8}
    \newcolumntype{g}{>{\columncolor{Green}}c}
    \newcolumntype{b}{>{\columncolor{Blue}}c}

    \setbeamertemplate{navigation symbols}{}
    \setbeamertemplate{footline}[frame number]
    \setbeamertemplate{itemize/enumerate body begin}{\tiny}
    \setbeamertemplate{itemize/enumerate subbody begin}{\tiny}
    \setbeamertemplate{navigation symbols}{}
}

\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{color}
\setbeamertemplate{caption}[numbered]
\usepackage{listings}
\usepackage{inputenc}
\setbeamerfont{subsubsection in toc}{size=\tiny}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes,arrows}
%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title{Kirsch-Lee WES Data Analysis}
\subtitle{Signature}

%\author{Dadong Zhang} % Your name
\institute % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
    DCI Shared Resources Bioinformatics \\ % Your institution for the title page
    \color{blue}{dcibioinformatics@duke.edu}\\
}
\date{\today} % Date, can be changed to a custom date

%----------------------------------------------------------------------------------------
%	DOCUMENT
%----------------------------------------------------------------------------------------
\begin{document}

<<globalsetup,echo=FALSE, include=FALSE>>=
rm(list=ls())
stdt<-date()
set.seed(275312)
options(tide=TRUE, width = 75, digits=3)
library(knitr)
library(xtable)
library(tibble)
library(ggplot2)
library(reshape2)
opts_chunk$set(
               dev='png',
               fig.align='center',
               fig.show='hold',
               size='tiny',
               fig.width=7, fig.height=7,
               out.width='.7\\linewidth',
               comment="",
               warning=FALSE,
               cache=FALSE,
               error=FALSE,
               message=FALSE,
               #include=FALSE,
               dpi=300
               )
@ 

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

<<localfuncs, echo=F>>=
printab <- function(tab, idx=1:20, cap=NULL, digits=2, align=NULL, scale = 0.7, ...){
  n <- min(max(idx), nrow(tab))
  tab <- as.data.frame(tab[1:n, ])
  pvars <- grep("pval|fdr|adj|p.val|q.val|p.geomean", colnames(tab), value = T)
  tab[,pvars] <- sapply(tab[,pvars], formatC, digits=2, format="e")
  print(xtable(tab, caption = cap, digits=digits, align=align),
      scale = scale,
      caption.placement = "top", ...)
}
getsummplot<-function(smtype="Frame_Shift_Del"){
    dat<-dplyr::filter(psumm, variable==smtype)
    ggplot(data=dat, aes(x=Tumor_Sample_Barcode, y=value))+
        geom_point(aes(colour=genotype), size=2)+
        facet_grid(~treatment, scales="free_x", space="free")+
        labs(y="Freq", x="", title=smtype)+
        theme(axis.text.x=element_text(angle=45, hjust=1, size=6)) 
}
get11pep<-function(pos, seq){
    substr(seq, pos-5, pos+5)
}
subchar <- function(string, pos, char="-") { 
        for(i in pos) { 
   			string <- gsub(paste("^(.{", i-1, "}).", sep=""), paste0("\\1", char), string) 
        } 
        string 
}
getmafsub<-function(masterdat, pdat, genotype){
    subsetMaf(masterdat,
              tsb=pdat$Tumor_Sample_Barcode[pdat$genotype==genotype],
              mafObj=T)
}

get.tnm<-function(mafobj, syn=TRUE){
    trinucleotideMatrix(maf = mafobj, ref_genome = "BSgenome.Mmusculus.UCSC.mm10", 
                               prefix = 'chr', add = TRUE, ignoreChr = 'MT', useSyn = syn)
}
getsubsig<-function(mafsub, pdat, genotype, syn=TRUE){
    tnm<-get.tnm(maf=mafsub, syn=syn)
    n<-length(pdat$Tumor_Sample_Barcode[pdat$genotype==genotype])-1
    sign<-extractSignatures(mat=tnm, nTry=n, plotBestFitRes=FALSE, parallel="P6")
    return(sign)
}
@

\tiny
%slide#2
\begin{frame}[fragile]
\frametitle{Overview} % Table of contents slide, comment this block out to remove it
\tableofcontents %
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------
%------------------------------------------------
\section{Global settings}
\begin{frame}[fragile]{Global settings}
<<echo=TRUE, results="markup", tidy=FALSE>>=
library(tidyverse)
library(BSgenome.Mmusculus.UCSC.mm10)
library(maftools, lib.loc="/home.local/dz33/R/x86_64-pc-linux-gnu-library/3.5")
library(ensembldb)
library(EnsDb.Mmusculus.v79)
library(AnnotationFilter)
library(readxl)
library(dendextend)
library(NMF)
library(pheatmap)
ref="/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/WholeExome/Data/sanger/GRCm38_68.fa"
wd<-"/data1/workspace/DCI/Kirsch/Chang-Lung.Lee/signature/"
maffile<-file.path(wd, "merged_maf/Changlung_merged_converted.maf")
@
\end{frame}

<<echo=F,results='hide'>>=
pdat<-read_excel("Kirsch-ChangLung-combined-manifest_CL.xlsx")%>%
    data.frame()%>%dplyr::select(-1)%>%
    mutate(genotype=gsub(" ", ":", genotype))%>%
    mutate(genotype=gsub(";", "", genotype), 
           type=tolower(type), 
           treatment=toupper(treatment))
@

<<forJianguo, echo=F, include=F, eval=F>>=
silent<-c("3'UTR", "5'UTR", "3'Flank", "Targeted_Region", "Silent", "Intron",
                       "RNA", "IGR", "Splice_Region", "5'Flank", "lincRNA", 
                       "De_novo_Start_InFrame", "De_novo_Start_OutOfFrame",
                       "Start_Codon_Ins", "Start_Codon_SNP", "Stop_Codon_Del")
nonSilent = c("Frame_Shift_Del", "Frame_Shift_Ins",
            "Splice_Site", "Translation_Start_Site", "Nonsense_Mutation",
            "Nonstop_Mutation", "In_Frame_Del", "In_Frame_Ins",
            "Missense_Mutation")
fj<-read.maf(maffile, clinicalData=pdat, removeDuplicatedVariants=TRUE, vc_nonSyn=c(silent, nonSilent))
pdats<-dplyr::filter(pdat, type=="sarcoma")
fjs<-subsetMaf(fj, tsb=pdats$Tumor_Sample_Barcode, mafObj=T)
#save(fjs, file="CL-Sarcoma-All.RData")
@

\begin{frame}[fragile]{Read MAF}
<<echo=TRUE, results="markup", tidy=FALSE>>=
tools::md5sum(maffile)
#master<-read.maf(maffile, clinicalData=pdat)
#save(master, file=file.path(wd, "/merged_maf/master_maf.RData"))
load(file=file.path(wd, "/merged_maf/master_maf.RData"))
#rm S45 as discussed and suggested by CL 111318
keepid<-with(pdat, Tumor_Sample_Barcode[Tumor_Sample_Barcode!="S45_T"])
master<-subsetMaf(master, tsb=keepid, mafObj=T)
@
\end{frame}

<<echo=FALSE, results="hide">>=
samp_summ<-merge(getSampleSummary(master)%>%data.frame(), pdat, all.x=T)
#write.csv(samp_summ, file.path(wd, "output/Lymphoma_sarcoma_sample_summary.csv"), quote=F)
gene_summ<-merge(getGeneSummary(master)%>%data.frame(), pdat, all.x=T)
#write.csv(gene_summ, file.path(wd, "output/Lymphoma_sarcoma_gene_summary.csv"), quote=F)
@

<<func, echo=F, include=FALSE>>=
get.titvhclust<-function(tt,pdat=pdats, prename="sarcoma_usesyn",syn=TRUE, ldg=ldg, leng, topl=T, topp=c(5, 30)){
    titvmtx<-dplyr::select(tt, matches("count|Barcode"))
    titvdat<-merge(titvmtx, pdat, by="Tumor_Sample_Barcode")
    dend.dat<-dplyr::select(titvdat, matches("count"))
    rownames(dend.dat)<-gsub("_T", "", titvdat$Tumor_Sample_Barcode)
    dend<-as.dendrogram(hclust(dist(dend.dat)))%>%set("branches_lwd", 1)
    col_bars<-ifelse(titvdat$genotype==lgd[1], "blue", 
                     ifelse(titvdat$genotype==lgd[2], "red", 
                            ifelse(titvdat$genotype==lgd[3], "green", "purple")))
    plotname<-file.path("./output", paste0(prename, "_titv_hclust.pdf"))
    #png(plotname, height=18, width=12, unit="cm", res=300)
    pdf(plotname, width=2.7, height=4)
    cex0=0.4
    par(mar = c(4,1,1,8), family="Helvetica", cex=cex0)
    plot(dend, horiz=T)
    colored_bars(colors = col_bars, dend = dend, rowLabels = "", horiz=T)
    if(topl){
        legend("topleft", legend = leng, fill = c("blue", "red", "green", "purple"), bty="n", cex=cex0)
    }else{
        legend(x=topp[1], y=topp[2], legend = leng, fill = c("blue", "red", "green", "purple"), bty="n", cex=cex0)
    }
    dev.off()
}

get.trihclust<-function(tridat, pdat=pdats, prename="sarcoma_usesyn", syn=TRUE, lgd, leng, topl=T, topp=c(5, 30)){
    dend.dat<-tridat%>%
        dplyr::select(tCw_to_A, tCw_to_T, tCw_to_G,
                      wGa_to_C, wGa_to_T, wGa_to_A)%>%
    data.frame()
    rownames(dend.dat)<-gsub("_T", "", tridat$Tumor_Sample_Barcode)
    dend<-as.dendrogram(hclust(dist(dend.dat)))%>%set("branches_lwd", 1)
    col_bars<-ifelse(tridat$genotype==lgd[1], "blue", 
                 ifelse(tridat$genotype==lgd[2], "red", 
                        ifelse(tridat$genotype==lgd[3], "green", "purple")))

    ##dendrogram
    plotname<-file.path("./output", paste0(prename, "_trinuc_hclust.pdf"))
    pdf(plotname, width=2.7, height=4)
    cex0=0.4
    par(mar = c(4,1,1,8), family="Helvetica", cex=cex0)
    plot(dend, horiz=T)
    colored_bars(colors = col_bars, dend = dend, rowLabels = "", horiz=T)
    if(topl){
        legend("topleft", legend = leng, fill = c("blue", "red", "green", "purple"), bty="n", cex=cex0)
    }else{
        legend(x=topp[1], y=topp[2], legend = leng, fill = c("blue", "red", "green", "purple"), bty="n", cex=cex0)
    }
    dev.off()
}

getglobalsig<-function(tnm, pdat){
    n<-length(pdat$Tumor_Sample_Barcode)-1
    sign<-extractSignatures(mat=tnm, nTry=n, plotBestFitRes=FALSE, parallel="P6")
    return(sign)
}
@

\section{Lymphoma}
\begin{frame}[fragile]{Experimental design}
<<echo=F,results='asis'>>=
pdatl<-dplyr::filter(pdat, type=="lymphoma")
printab(pdatl, idx=1:nrow(pdatl), digits=0, scale=0.6)
@
\end{frame}

\subsection{Summary}
\begin{frame}[fragile]{Summary}
<<echo=FALSE, results="markup">>=
master_l<-subsetMaf(master, tsb=pdatl$Tumor_Sample_Barcode, mafObj=T)
master_l
library(writexl)
#write_xlsx(master_l@data, "Lynphoma_mutations_full.xlsx")
@
\end{frame}


\begin{frame}[fragile]{Summary}
<<echo=FALSE, results="asis">>=
plotmafSummary(maf = master_l, rmOutlier = FALSE, showBarcodes=TRUE, addStat="median",
               textSize=0.5, dashboard = TRUE, titvRaw = FALSE)
@
\end{frame}

\subsection{Summary by mutational categories}
\begin{frame}[fragile]{Summary by sample}
<<echo=FALSE, results="asis">>=
psumm<-merge(getSampleSummary(master_l)%>%data.frame(), pdatl, all.x=T)%>%
    dplyr::select(-type)%>%
    melt(id=c("Tumor_Sample_Barcode", "genotype", "treatment"))%>%
    mutate_at(2:3, as.factor)%>%
    mutate_at(c(1,4), as.character)
getsummplot("total")
@
\end{frame}

\begin{frame}[fragile]{Test among genotypes}
<<echo=FALSE, results="asis">>=
totmt<-dplyr::filter(psumm, variable=="total")
totmt%>%group_by(genotype, treatment)%>%
    summarise(sum_mt=sum(value))
mut_count<-totmt$value
genotype<-factor(totmt$genotype, 
                 levels=c("Non-IR:p53-/-", "IR-induced:p53+/-", 
                          "IR-induced:p53:WT", "IR-induced:KrasLA1"))
pairwise.wilcox.test(mut_count, genotype)
@
\end{frame}


\begin{frame}[fragile]{Summary by sample}
<<echo=FALSE, results="asis">>=
vars<-unique(psumm$variable)
getsummplot("Missense_Mutation")
@
\end{frame}

\begin{frame}[fragile]{Summary by sample}
<<echo=FALSE, results="asis">>=
getsummplot("Nonsense_Mutation")
@
\end{frame}
i
\begin{frame}[fragile]{Summary by sample}
<<echo=FALSE, results="asis">>=
getsummplot("Splice_Site")
@
\end{frame}

\begin{frame}[fragile]{Oncoplot}
<<echo=FALSE, results="asis">>=
oncoplot(maf = master_l, top=10, showTumorSampleBarcodes=T,clinicalFeatures="treatment", 
         SampleNamefontSize=5, sortByAnnotation=TRUE, writeMatrix=TRUE)
@
\end{frame}

\begin{frame}[fragile]{Oncoplot}
<<echo=FALSE, results="asis">>=
oncoplot(maf = master_l, top=10, showTumorSampleBarcodes=T,clinicalFeatures="genotype", 
         SampleNamefontSize=5, sortByAnnotation=TRUE, writeMatrix=TRUE)
@
\end{frame}


\begin{frame}[fragile]{Oncoplot}
<<echo=FALSE, results="asis">>=
master_l.tt<-titv(maf=master_l, plot=F, useSyn=TRUE)
ltt<- master_l.tt%>%Reduce(function(df1, df2) full_join(df1, df2, by="Tumor_Sample_Barcode"), .)
colnames(ltt)<-gsub(".x", ".prop", colnames(ltt))
colnames(ltt)<-gsub(".y", ".count", colnames(ltt))
write.csv(ltt, "./output/Lymphoma_TiTv_matrix.csv")
plotTiTv(res=master_l.tt, showBarcodes=TRUE)
@
\end{frame}

<<trimutl, echo=F, include=FALSE>>=
#tnm<-get.tnm(mafobj=master_l, syn=TRUE)
lgd<-c("IR-induced:p53:WT","IR-induced:KrasLA1", "IR-induced:p53+/-","Non-IR:p53-/-")
leng<-c("IR-induced p53 WT", "IR-induced Kras LA1",
        as.expression(bquote("IR-induced p53"^"+/-")), 
        as.expression(bquote("Non-IR p53"^"-/-")))
tridat<-read.csv("./output/lymphoma_usesyn_nmf_matrix.csv", head=T, stringsAsFactors=FALSE)
get.trihclust(tridat, pdat=pdatl, prename="lymphoma_usesyn", syn=TRUE, lgd=lgd, topl=F, leng=leng, topp=c(5.5, 33))
@

\begin{frame}[fragile]{Lymphoma signature analysis}
<<echo=TRUE, results="asis">>=
#invisible(capture.output(sig<-suppressMessages(getglobalsig(tnm=tnm, pdat=pdatl))))
#save(sig, file="./output/Lymphoma_global_signature.RData")
load("./output/Lymphoma_global_signature.RData")
@
\end{frame}

\begin{frame}[fragile]{Lymphoma signature analysis}
<<echo=FALSE, results="markup">>=
head(sig[["signatures"]], n=16)
@
\end{frame}

\begin{frame}[fragile]{Lymphoma signature analysis}
<<echo=FALSE, results="asis">>=
plotSignatures(sig, show_barcodes=TRUE, yaxisLim=NA, title_size=0.8) 
@
\end{frame}

\begin{frame}[fragile]{Lymphoma signature analysis}
<<echo=FALSE, results="asis">>=
pheatmap::pheatmap(mat = sig$coSineSimMat, 
                            cluster_rows = FALSE,
                            main = "cosine similarity against validated signatures")
@
\end{frame}

\section{Session Information}
\begin{frame}[fragile]
<<echo=FALSE,results='asis'>>=
toLatex(sessionInfo(), locale=FALSE)
print(paste("Start Time:  ",stdt))
print(paste("End Time:  ",date()))
@ 
\end{frame}

\end{document}
